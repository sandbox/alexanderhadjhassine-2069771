
Set Cron Time module
-----------------
By Alexander Hadj Hassine, alexander@hadj-hassine.de

This module made it possible to fine-tune the maximum execution time for cron.
The main problem is that Drupal set a fix maximum value of 240 seconds in 
includes/common.inc for cron. When you get a Message like: "PHP Fatal error:  
Maximum execution time of 240 seconds" has you reached the Drupal limitation.